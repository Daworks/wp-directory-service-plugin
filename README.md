# DW Directory Service
이 플러그인은 워드프레스에서 디렉토리 서비스를 할 수 있도록 고안되었습니다.
지역 커뮤니티 사이트에서 지역의 맛집 등을 소개하는 용도나 교회 홈페이지에서 교우 사업장을 소개하는 용도로 사용하려고 개발했는데 사용하기 나름이겠지요.
  
  
이 플러그인의 repository는 github로 옮겨졌습니다.  
[Github으로 바로가기](https://github.com/Daworks/dw-directory)
